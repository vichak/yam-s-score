const admin = require('firebase-admin');
const FieldValue = admin.firestore.FieldValue;

// secret telechagé sur firebase console
const serviceAccount = require('./yam-s-score-54800ae1adc5.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

async function createRount(round) {
  const res = await db.collection('rounds').add(round);
  console.log('Added round with ID: ', res.id);
}

async function migrateGame(gameId) {
  const doc = await db.collection('games').doc(gameId).get();
  const game = doc.data();
  game.rounds.forEach(round => {
    round.gameId = game.id;
    createRount(round);
  });
  const docRef = db.collection('games').doc(gameId);
  const res = await docRef.update({
    rounds: FieldValue.delete()
  });
}

async function migrate() {
  const snapshot = await db.collection('games').get();
  snapshot.docs.map(game => {
    console.log(game.data().id);
    migrateGame(game.data().id);
  });
};

migrate();

