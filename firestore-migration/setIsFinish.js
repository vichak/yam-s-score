const admin = require('firebase-admin');

// secret telechagé sur firebase console
const serviceAccount = require('./yam-s-score-54800ae1adc5.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

async function updateRound(roundId, isFinish) {
  const docRef = db.collection('rounds').doc(roundId);
  const res = await docRef.update({
    isFinish: isFinish
  });
}

async function migrate() {
  const citiesRef = db.collection('rounds');
  const snapshot = await citiesRef.get();
  if (snapshot.empty) {
    console.log('No matching documents.');
    return;
  }

  snapshot.forEach(doc => {
    var round = doc.data();
    let isFinish = true;
    if (round.lowerSectionCategories) {
      round.lowerSectionCategories.some(cat => {
        if (cat.isPlayed === false) {
          round.isFinish = false;
          return false;
        }
      });
    }
    if (round.upperSectionCategories) {
      round.upperSectionCategories.some(cat => {
        if (cat.isPlayed === false) {
          isFinish = false;
          return false;
        }
      });
    }
    console.log(doc.id, '=>', isFinish);
     updateRound(doc.id, isFinish);
  });
};

migrate();

