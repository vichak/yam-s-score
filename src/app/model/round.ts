import { Category } from './category';
import { Player } from './player';

export class Round {
  id: string;
  gameId: string;
  isFinish: boolean;
  player: Player;
  bonus = 35;
  bonusCondition = 63;
  bonusScores = `${this.bonus} points. Si total de la partie supérieure >= ${this.bonusCondition}`;
  upperSectionCategories: Category[] = [];
  lowerSectionCategories: Category[] = [];
  total = 0;

  constructor(player: Player, gameId: string) {
    this.gameId = gameId;
    this.isFinish = false;
    this.player = player;
    this.upperSectionCategories.push(
      new Category('As', '1 x le nombre de dés 1 obtenus', [0, 1, 2, 3, 4, 5])
    );
    this.upperSectionCategories.push(
      new Category('Deux', '2 x le nombre de dés 2 obtenus', [
        0,
        2,
        4,
        6,
        8,
        10,
      ])
    );
    this.upperSectionCategories.push(
      new Category('Trois', '3 x le nombre de dés 3 obtenus', [
        0,
        3,
        6,
        9,
        12,
        15,
      ])
    );
    this.upperSectionCategories.push(
      new Category('Quatre', '4 x le nombre de dés 4 obtenus', [
        0,
        4,
        8,
        12,
        16,
        20,
      ])
    );
    this.upperSectionCategories.push(
      new Category('Cinq', '5 x le nombre de dés 5 obtenus', [
        0,
        5,
        10,
        15,
        20,
        25,
      ])
    );
    this.upperSectionCategories.push(
      new Category('Six', '6 x le nombre de dés 6 obtenus', [
        0,
        6,
        12,
        18,
        24,
        30,
      ])
    );
    this.lowerSectionCategories.push(
      new Category('Brelan', '3 dés identiques; Total des 5 dés', [
        0,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
      ])
    );
    this.lowerSectionCategories.push(
      new Category('Carré', '4 dés identiques; Total des 5 dés', [
        0,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
      ])
    );
    this.lowerSectionCategories.push(
      new Category('Full', '3 dés identiques et 2 dés identiques', [0, 25])
    );
    this.lowerSectionCategories.push(
      new Category('Petite suite', 'suite avec 4 dés', [0, 15])
    );
    this.lowerSectionCategories.push(
      new Category('Grande suite', 'suite avec 5 dés', [0, 30])
    );
    this.lowerSectionCategories.push(
      new Category('Yams', '5 dés identiques', [0, 50])
    );
    this.lowerSectionCategories.push(
      new Category('Chance', 'Total des 5 dés', [
        0,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
      ])
    );
  }
}
