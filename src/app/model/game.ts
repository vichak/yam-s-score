export class Game {
  id: string;
  startTime: Date;
  endTime: Date;
  nextGameId: string;
  previousGameId: string;
  description: string
}
