export class Category {
  id: number;
  name: string;
  scoresDescription: string;
  scores = 0;
  isPlayed = false;
  possibleValues: number[] | undefined;

  constructor(
    name: string,
    scoresDescription: string,
    possibleValues?: number[]
  ) {
    this.name = name;
    this.scoresDescription = scoresDescription;
    this.possibleValues = possibleValues;
  }
}
