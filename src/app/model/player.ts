export class Player {
  id: number;
  name: string;
  order: number;
  constructor(name: string) {
    this.name = name;
  }
}
