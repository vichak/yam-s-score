
export class HistoryGame {
  id: string;
  gameId: string;
  date: Date;
  text: string;

  constructor(gameId: string, text: string) {
    this.date = new Date();
    this.gameId = gameId;
    this.text = text
  }
}
