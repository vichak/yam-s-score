import { Component, Input, OnInit } from '@angular/core';
import { Round } from './../model/round';


@Component({
  selector: 'app-round-list',
  templateUrl: './round-list.component.html',
  styleUrls: ['./round-list.component.css'],
})
export class RoundListComponent implements OnInit {
  @Input()
  rounds: Round[] = [];

  constructor() { }

  ngOnInit(): void { }
}
