import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HistoryGameService } from '../history-game.service';
import { HistoryGame } from '../model/history-game';
import { ScreenWakeLockService } from '../screen-wake-lock.service';
import { DialogInputComponent } from './../dialog-input/dialog-input.component';
import { GameService } from './../game.service';
import { Category } from './../model/category';
import { Game } from './../model/game';
import { Player } from './../model/player';
import { Round } from './../model/round';
import { RoundService } from './../round.service';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
})
export class GameComponent implements OnInit, OnDestroy {
  game: Game;
  rounds: Round[] = [];
  upperCategories: Category[] = [];
  lowerCategories: Category[] = [];
  historyGames: HistoryGame[] = [];

  private unsubscribe = new Subject();

  constructor(
    private gameService: GameService,
    private roundService: RoundService,
    private historyGameService: HistoryGameService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public router: Router,
    public screenWakeLockService: ScreenWakeLockService
  ) { }

  ngOnInit() {
    this.route.params.pipe(takeUntil(this.unsubscribe)).subscribe(() => {
      const id = this.route.snapshot.paramMap.get('id');
      if (id == null) return this.router.navigate(['home']);
      this.gameService.getGame(id).subscribe((g) => {
        if (g === undefined) {
          return this.router.navigate(['404']);
        }
        this.game = g;
        this.roundService.getRounds(g.id).subscribe((rounds) => {
          if (rounds === undefined) {
            return this.router.navigate(['404']);
          }
          this.rounds = rounds.sort((a, b) => a.player.order - b.player.order);
          this.upperCategories = this.rounds[0].upperSectionCategories;
          this.lowerCategories = this.rounds[0].lowerSectionCategories;
          this.screenWakeLockService.requestWakeLock();
        });
      });
      this.historyGameService.getHistoryForGame(id).pipe(takeUntil(this.unsubscribe))
        .subscribe((historyGames) => this.historyGames = historyGames);
    });
  }

  ngOnDestroy() {
    this.screenWakeLockService.releaseWakeLock();
    this.unsubscribe.next(undefined);
  }

  getPlayerScoresUpperCategories(player: Player) {
    const playerRound = this.rounds.find((round) => round.player === player);
    return this.getRoundScoresUpperCategories(playerRound);
  }

  getRoundScoresUpperCategories(playerRound: Round | undefined): number {
    if (playerRound == undefined) return 0;
    return playerRound.upperSectionCategories
      .map((c) => c.scores)
      .reduce((a, b) => Number(a) + Number(b), 0);
  }

  getPlayerBonus(player: Player): number {
    const playerRound = this.rounds.find((round) => round.player === player);
    if (playerRound == undefined) return 0;
    const total = this.getRoundScoresUpperCategories(playerRound);
    if (total < playerRound.bonusCondition) {
      return total - playerRound.bonusCondition;
    } else {
      return playerRound.bonus;
    }
  }

  getRoundBonus(playerRound: Round) {
    let result = 0;
    const total = this.getRoundScoresUpperCategories(playerRound);
    if (total >= playerRound.bonusCondition) {
      result = playerRound.bonus;
    }
    return result;
  }

  getPlayerUpperScore(playerRound: Round) {
    return (
      this.getRoundScoresUpperCategories(playerRound) +
      this.getRoundBonus(playerRound)
    );
  }

  getUpperSectionCategories(round: Round, categoryName: string): number {
    const r = round.upperSectionCategories.find((c) => c.name === categoryName);
    return r ? r.scores : 0;
  }

  getLowerSectionCategories(round: Round, categoryName: string): number {
    const r = round.lowerSectionCategories.find((c) => c.name === categoryName);
    return r ? r.scores : 0;
  }

  getPlayerLowerScore(playerRound: Round) {
    return playerRound.lowerSectionCategories
      .map((c) => c.scores)
      .reduce((a, b) => Number(a) + Number(b), 0);
  }

  getTotalRound(round: Round) {
    return this.getPlayerLowerScore(round) + this.getPlayerUpperScore(round);
  }

  isPlayedUpperCategories(round: Round, categoryName: string): boolean {
    const category = round.upperSectionCategories.find(
      (c) => c.name === categoryName
    );
    return category ? category.isPlayed : false;
  }

  isPlayedLowerCategories(round: Round, categoryName: string): boolean {
    const category = round.lowerSectionCategories.find(
      (c) => c.name === categoryName
    );
    return category ? category.isPlayed : false;
  }

  openDialog(round: Round, categoryName: string): void {
    let _category = round.upperSectionCategories.find(
      (c) => c.name === categoryName
    );
    if (_category === undefined) {
      _category = round.lowerSectionCategories.find(
        (c) => c.name === categoryName
      );
    }
    if (_category === undefined) {
      console.error(`category not found : ${categoryName}`);
      return;
    }
    const category = _category;

    const dialogRef = this.dialog.open(DialogInputComponent, {
      data: { category, round },
    });

    dialogRef.afterClosed().subscribe((categoryPlayed) => {
      if (categoryPlayed === undefined || categoryPlayed.scores === undefined) {
        return;
      }
      category.scores = categoryPlayed.scores;
      category.isPlayed = categoryPlayed.isPlayed;

      // persite data
      this.roundService.update(round);
      this.historyGameService.log(round.gameId, round.player.name, categoryPlayed);

      // c'est la fin du jeu?
      if (this.getGameProgess() === 100) {
        this.game.endTime = new Date();
        this.gameService.update(this.game);

        this.rounds.forEach((r) => {
          r.total = this.getTotalRound(r);
          r.isFinish = true;
          this.roundService.update(r);
        });
        this.screenWakeLockService.releaseWakeLock();
      }
    });
  }

  getGameProgess() {
    let nbTotalToPlay = 0;
    let nbTotalPlayed = 0;
    this.rounds.forEach((round) => {
      round.upperSectionCategories.forEach((cat) => {
        nbTotalToPlay++;
        if (cat.isPlayed) {
          nbTotalPlayed++;
        }
      });
      round.lowerSectionCategories.forEach((cat) => {
        nbTotalToPlay++;
        if (cat.isPlayed) {
          nbTotalPlayed++;
        }
      });
    });
    if (nbTotalToPlay === 0) {
      return 0;
    }
    return (nbTotalPlayed * 100) / nbTotalToPlay;
  }

  getWinner(): Player {
    let winnerRound: Round = new Round(new Player(""), "");
    let lastRoundScore = 0;
    this.rounds.forEach((round) => {
      const roundScore = this.getTotalRound(round);
      if (lastRoundScore < roundScore) {
        winnerRound = round;
        lastRoundScore = roundScore;
      }
    });
    return winnerRound?.player;
  }

  replay() {
    let newGame = new Game();
    newGame.startTime = new Date();
    newGame.previousGameId = this.game.id;
    newGame = this.gameService.addGame(newGame);
    this.game.nextGameId = newGame.id;
    this.gameService.update(this.game);
    this.rounds.forEach((round) =>
      this.roundService.addRound(new Round(round.player, newGame.id))
    );
    this.router.navigate(['/partie', newGame.id]);
  }
}
