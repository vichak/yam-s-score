import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { environment } from './../environments/environment';
import { ScreenWakeLockService } from './screen-wake-lock.service';
import { UpdateService } from './update.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  webNavigator: any = null;
  isNavigationShare = false;
  currentApplicationVersion = environment.appVersion;
  isScreenWakeLockSupported = false;
  isScreenWakeLocked = false;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private screenWakeLockService: ScreenWakeLockService, private updateService: UpdateService) {
    this.webNavigator = window.navigator;
    this.isScreenWakeLockSupported = screenWakeLockService.isSupported();
    this.isScreenWakeLocked = screenWakeLockService.isLocked;
  }

  ngOnInit() {
    if (this.webNavigator && this.webNavigator.share) {
      this.isNavigationShare = true;
    }
    this.screenWakeLockService.isLockedChange.subscribe(value => this.isScreenWakeLocked = value);
  }

  requestWakeLock() {
    this.screenWakeLockService.requestWakeLock();
  }

  releaseWakeLock() {
    this.screenWakeLockService.releaseWakeLock();
  }

  navigatorShare() {
    if (this.isNavigationShare) {
      this.webNavigator.share({
        title: "Yam's score",
        text: "Hey! viens voir c'est trop bien",
        url: this.getUrlToShare(),
      });
    }
  }

  getUrlToShare() {
    return window.location.href;
  }
}
