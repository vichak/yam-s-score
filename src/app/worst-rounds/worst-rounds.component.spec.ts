import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorstRoundsComponent } from './worst-rounds.component';

describe('WorstRoundsComponent', () => {
  let component: WorstRoundsComponent;
  let fixture: ComponentFixture<WorstRoundsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WorstRoundsComponent]
    });
    fixture = TestBed.createComponent(WorstRoundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
