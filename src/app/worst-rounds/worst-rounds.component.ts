import { Component, OnInit } from '@angular/core';
import { Round } from './../model/round';
import { RoundService } from './../round.service';


@Component({
  selector: 'app-worst-rounds',
  templateUrl: './worst-rounds.component.html',
  styleUrls: ['./worst-rounds.component.css'],
})
export class WorstRoundsComponent implements OnInit {
  rounds: Round[] = [];

  constructor(private roundService: RoundService) { }

  ngOnInit(): void {
    this.roundService
      .getWorstRounds()
      .subscribe((rounds) => (this.rounds = rounds));
  }
}
