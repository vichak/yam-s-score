import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { filter } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UpdateService {
  constructor(private swUpdate: SwUpdate, private snackbar: MatSnackBar) {
    this.swUpdate.versionUpdates
      .pipe(filter((evt): evt is VersionReadyEvent => evt.type === 'VERSION_READY')).subscribe(evt => {
        const snack = this.snackbar.open('🎉 Nouvelle version dispo !!! ', 'yeah');
        snack
          .onAction()
          .subscribe(() => {
            window.location.reload();
          });
      });
  }
}