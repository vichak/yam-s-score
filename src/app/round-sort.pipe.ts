import { Pipe, PipeTransform } from '@angular/core';
import { Round } from './model/round';


@Pipe({
  name: 'roundSort',
})
export class RoundSortPipe implements PipeTransform {
  transform(rounds: Round[]): Round[] {
    const tmpRounds = rounds.map((round) => {
      const r = new Round(round.player, round.gameId);
      r.total = round.total;
      r.player.name = round.player.name;
      return r;
    });
    return tmpRounds.sort((round1, round2): number => {
      if (round1.total > round2.total) {
        return -1;
      }
      if (round1.total < round2.total) {
        return 1;
      }
      return 0;
    });
  }
}
