import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BestRoundsComponent } from './best-rounds/best-rounds.component';
import { GameComponent } from './game/game.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PlayersComponent } from './players/players.component';
import { RulesComponent } from './rules/rules.component';
import { WorstRoundsComponent } from './worst-rounds/worst-rounds.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'nouvelle-partie', component: PlayersComponent },
  { path: 'meilleurs-lancers', component: BestRoundsComponent },
  { path: 'pires-lancers', component: WorstRoundsComponent },
  { path: 'regles', component: RulesComponent },
  { path: 'partie/:id', component: GameComponent },
  { path: 'partie/:id/joueurs', component: PlayersComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
