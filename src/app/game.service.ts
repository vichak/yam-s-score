import { Injectable } from '@angular/core';
import { collection, doc, docData, DocumentReference, DocumentSnapshot, Firestore, onSnapshot, setDoc } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { Game } from './model/game';


@Injectable({
  providedIn: 'root',
})
export class GameService {

  constructor(private readonly firestore: Firestore) {
  }

  getGame(id: string): Observable<Game> {
    let getGame$ = new Subject<Game>();
    const documentReference: DocumentReference<Game> = doc(this.firestore, "games", id) as DocumentReference<Game>;
    onSnapshot<Game>(documentReference, (doc) => {
      if (doc.exists()) getGame$.next(doc.data())
    });
    return getGame$.asObservable();
  }

  update(game: Game): void {
    const documentReference = doc(this.firestore, "games", game.id);
    setDoc(documentReference, game);
  }

  addGame(game: Game): Game {
    const documentReference = doc(collection(this.firestore, "games"));
    game.id = documentReference.id;
    setDoc(documentReference, this.getGameForFireStore(game));
    return game;
  }

  private getGameForFireStore(game: Game) {
    return JSON.parse(JSON.stringify(game));
  }
}
