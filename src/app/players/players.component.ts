import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GameService } from '../game.service';
import { Game } from '../model/game';
import { Player } from '../model/player';
import { Round } from '../model/round';
import { RoundService } from '../round.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css'],
})
export class PlayersComponent implements OnInit, OnDestroy {
  game: Game;
  newPlayerName = '';
  newPlayerNameAlreadyExists = false;
  players: Player[] = [];
  isNewGame = true;
  rounds: Round[] = [];
  originalPlayers: Player[] = [];
  showBackToGame = false;

  constructor(
    private gameService: GameService,
    private roundService: RoundService,
    public router: Router,
    private route: ActivatedRoute
  ) { }

  private unsubscribe = new Subject();

  ngOnDestroy() {
    this.unsubscribe.next(undefined);
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.unsubscribe)).subscribe((val) => {
      const id = this.route.snapshot.paramMap.get('id');
      this.game = new Game();
      if (id === null) {
        this.game.startTime = new Date();
      } else {
        this.showBackToGame = true;
        this.game.id = id;
        this.gameService.getGame(id).subscribe((game) => this.game = game);
        this.roundService.getRounds(id).subscribe((rounds) => {
          if (rounds !== undefined) {
            this.rounds = rounds.sort((a, b) => a.player.order - b.player.order);
            this.players = rounds.map((round) => round.player);
            this.originalPlayers = [...this.players];
            this.isNewGame = false;
          }
        });
      }
    });
  }

  onNewPlayerNameSubmit() {
    if (this.newPlayerName === '') {
      return;
    }
    if (this.players.map((p) => p.name).includes(this.newPlayerName)) {
      this.newPlayerNameAlreadyExists = true;
      return;
    } else {
      this.newPlayerNameAlreadyExists = false;
    }
    const newPlayer = new Player(this.newPlayerName);
    newPlayer.order = this.players.length;
    this.players.push(newPlayer);
    this.newPlayerName = '';
  }

  removePlayer(player: Player) {
    this.players = this.players.filter((p) => p.name !== player.name);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.players, event.previousIndex, event.currentIndex);
  }

  letsgo() {
    if (this.isNewGame) {
      this.game = this.gameService.addGame(this.game);
      this.players.forEach((player) =>
        this.roundService.addRound(new Round(player, this.game.id))
      );
    } else {
      this.gameService.update(this.game);
      const playersToAdd = this.players.filter(
        (player: Player) => this.originalPlayers.indexOf(player) === -1
      );
      playersToAdd.forEach((player) => {
        player.order = this.players.indexOf(player);
        this.roundService.addRound(new Round(player, this.game.id));
      });

      const playersToRemove = this.originalPlayers.filter(
        (player) => this.players.indexOf(player) === -1
      );
      const roundsToRemove = this.rounds.filter((round) =>
        playersToRemove.includes(round.player)
      );
      roundsToRemove.forEach((round) => this.roundService.removeRound(round));

      // update player order
      this.players.forEach((player, index) => this.players[index].order = index);
      this.players.forEach((player) => {
        this.rounds.forEach(round => {
          if (player.name == round.player.name) {
            round.player.order = player.order;
            this.roundService.update(round);
          }
        })
      });
    }
    this.router.navigate(['/partie', this.game.id]);
  }
}
