import { Component, OnInit } from '@angular/core';
import { Round } from './../model/round';
import { RoundService } from './../round.service';


@Component({
  selector: 'app-best-rounds',
  templateUrl: './best-rounds.component.html',
  styleUrls: ['./best-rounds.component.css'],
})
export class BestRoundsComponent implements OnInit {
  rounds: Round[] = [];

  constructor(private roundService: RoundService) { }

  ngOnInit(): void {
    this.roundService
      .getBestRounds()
      .subscribe((rounds: Round[]) => (this.rounds = rounds));
  }
}
