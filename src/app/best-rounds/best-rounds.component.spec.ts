import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BestRoundsComponent } from './best-rounds.component';

describe('BestRoundsComponent', () => {
  let component: BestRoundsComponent;
  let fixture: ComponentFixture<BestRoundsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BestRoundsComponent]
    });
    fixture = TestBed.createComponent(BestRoundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
