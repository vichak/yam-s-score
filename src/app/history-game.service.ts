import { Injectable } from '@angular/core';
import { collectionData, CollectionReference, doc, Firestore, setDoc, where } from '@angular/fire/firestore';
import { collection, orderBy, query } from '@firebase/firestore';
import { Observable } from 'rxjs';
import { Category } from './model/category';
import { HistoryGame } from './model/history-game';


@Injectable({
  providedIn: 'root',
})
export class HistoryGameService {
  private historyCollection: CollectionReference;

  constructor(private readonly firestore: Firestore) {
    this.historyCollection = collection(this.firestore, 'historyGame');
  }

  getHistoryForGame(gameId: string): Observable<HistoryGame[]> {
    const queryHistory = query(this.historyCollection, where('gameId', '==', gameId), orderBy('date', 'desc'));
    return collectionData(queryHistory) as Observable<HistoryGame[]>;
  }

  log(gameId: string, playerName: string, categoryPlayed: Category): void {
    const documentReference = doc(this.historyCollection);
    const text = `${playerName} a mis ${categoryPlayed.scores} dans ${categoryPlayed.name}`;
    let history = new HistoryGame(gameId, text);
    setDoc(documentReference, this.getHistoryForFireStore(history));
  }

  private getHistoryForFireStore(historyRound: HistoryGame) {
    return JSON.parse(JSON.stringify(historyRound));
  }
}
