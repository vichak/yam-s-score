import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Category } from './../model/category';
import { DialogData } from './dialog-data';


@Component({
  selector: 'app-dialog-input',
  templateUrl: './dialog-input.component.html',
  styleUrls: ['./dialog-input.component.css'],
})
export class DialogInputComponent implements OnInit, AfterViewInit {
  @ViewChild('inputNumber')
  inputNumber: ElementRef;
  category: Category;

  constructor(
    public dialogRef: MatDialogRef<DialogInputComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
    this.category = { ...this.data.category };
    if (this.category.possibleValues === undefined && this.category.scores === 0 && !this.category.isPlayed) {
      this.category.scores = 0;
    }
  }

  ngAfterViewInit() {
    if (this.inputNumber && this.inputNumber.nativeElement) {
      this.inputNumber.nativeElement.focus();
    }
  }

  onPlayed(item: number) {
    this.category.isPlayed = true;
    this.category.scores = item;
    this.returnCategory();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  returnCategory(): void {
    this.dialogRef.close(this.category);
  }

  undo(): void {
    this.category.isPlayed = false;
    this.category.scores = 0;
    this.returnCategory();
  }
}
