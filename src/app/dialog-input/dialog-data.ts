import { Category } from './../model/category';
import { Round } from './../model/round';

export interface DialogData {
  round: Round;
  category: Category;
}
