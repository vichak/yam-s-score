import { Injectable } from '@angular/core';
import { collectionData, CollectionReference, deleteDoc, doc, Firestore, setDoc, where } from '@angular/fire/firestore';
import { collection, limit, orderBy, query } from '@firebase/firestore';
import { Observable } from 'rxjs';
import { Round } from './model/round';


@Injectable({
  providedIn: 'root',
})
export class RoundService {
  private roundCollection: CollectionReference;

  constructor(private readonly firestore: Firestore) {
    this.roundCollection = collection(this.firestore, 'rounds');
  }

  getRounds(gameId: string): Observable<Round[]> {
    const roundsQuery = query(this.roundCollection, where('gameId', '==', gameId));
    return collectionData(roundsQuery) as Observable<Round[]>;
  }

  update(round: Round): void {
    const documentReference = doc(this.firestore, "rounds", round.id);
    setDoc(documentReference, round);
  }

  addRound(round: Round): Round {
    const documentReference = doc(this.roundCollection);
    round.id = documentReference.id;
    setDoc(documentReference, this.getRoundForFireStore(round));
    return round;
  }

  removeRound(round: Round) {
    const documentReference = doc(this.firestore, "rounds", round.id);
    deleteDoc(documentReference);
  }

  getBestRounds(): Observable<Round[]> {
    const bestRoundsQuery = query(this.roundCollection, where('isFinish', '==', true), orderBy('total', "desc"), limit(100));
    return collectionData(bestRoundsQuery) as Observable<Round[]>;
  }

  getWorstRounds(): Observable<Round[]> {
    const bestRoundsQuery = query(this.roundCollection, where('isFinish', '==', true), orderBy('total', "asc"), limit(100));
    return collectionData(bestRoundsQuery) as Observable<Round[]>;
  }

  private getRoundForFireStore(round: Round) {
    return JSON.parse(JSON.stringify(round));
  }
}
