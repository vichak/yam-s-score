import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

// https://web.dev/wakelock/
@Injectable({
  providedIn: 'root',
})
export class ScreenWakeLockService {
  wakeLock: any = null;
  webNavigator: any = null;
  isLocked = false;
  isLockedChange: Subject<boolean> = new Subject<boolean>();

  constructor() {
    this.webNavigator = window.navigator;
    this.isLockedChange.subscribe(value => this.isLocked = value);
  }

  isSupported(): boolean {
    if (
      this.webNavigator &&
      this.webNavigator.wakeLock &&
      this.wakeLock === null
    ) {
      return true;
    } else {
      return false;
    }
  }

  async requestWakeLock() {
    if (this.isSupported()) {
      try {
        this.wakeLock = await this.webNavigator.wakeLock.request('screen');
        if (this.wakeLock != null) {
          this.wakeLock.addEventListener('release', () => {
            this.isLockedChange.next(false);
            console.log('Screen Wake Lock was released');
          });
          this.isLockedChange.next(true);
          console.log('Screen Wake Lock is active');
        }
      } catch (err: any) {
        console.error(`${err.name}, ${err.message}`);
      }
    } else {
      console.log('Screen Wake Lock is not supported');
    }
  }

  releaseWakeLock() {
    if (this.wakeLock) {
      this.wakeLock.release();
      this.wakeLock = null;
      this.isLockedChange.next(false);
    }
  }
}
